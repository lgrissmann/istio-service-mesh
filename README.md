
[[_TOC_]]

# O que é service mesh?
Uma service mesh é uma maneira de controlar como diferentes componentes de uma aplicação compartilham dados entre si. Diferentemente de outros sistemas de gerenciamento de comunicação, a service mesh é uma camada de infraestrutura dedicada, incorporada diretamente em uma aplicação. Essa camada de infraestrutura visível pode documentar o desempenho das interações entre os diferentes componentes da aplicação, facilitando a otimização da comunicação e evitando o tempo de inatividade, conforme a aplicação evolui.[^1]

![Sidecar](img/sidecar.png)
Figura 1: Representação do ___sidecar___

Em uma service mesh, as solicitações são encaminhadas entre microsserviços utilizando proxies em uma camada de infraestrutura própria. Por esse motivo, os proxies que compõem uma service mesh são, às vezes, chamados de "sidecars", pois são executados paralelamente a cada serviço, não dentro dele. Juntos, esses proxies "sidecars", desacoplados de cada serviço, formam uma rede em malha. [^1]

# O que é Istio?
Istio é uma plataforma de service mesh open source que inclui APIs para que ele possa se integrar a qualquer plataforma de geração de registros, sistema de telemetria ou sistema de políticas. Ele foi desenvolvido para ser executado em ambientes variados: on-premises, hospedado em nuvem, em containers do Kubernetes, em serviços executados em máquinas virtuais e outros.[^2]

![Istio Architecture](img/arch.svg)
Figura 2: ___Istio Architecture___ [^3]

A arquitetura do Istio está dividida em plano de dados e plano de controle. No plano de dados, é necessário implantar um proxy sidecar no ambiente para adicionar a compatibilidade com o Istio ao serviço. Esse proxy sidecar reside ao lado de um microsserviço e encaminha solicitações de/para outros proxies. Juntos, esses proxies formam uma rede mesh que intercepta a comunicação entre microsserviços. O plano de controle gerencia e configura proxies para encaminhar o tráfego. Ele também configura os componentes que aplicam políticas e coletam telemetria.[^2]

Com uma service mesh como o Istio, as equipes de desenvolvimento e operações estão melhor preparadas para lidar com a mudança de aplicações monolíticas para aplicações nativas em nuvem (coleções de aplicações de microsserviços pequenas, independentes e levemente acopladas). O Istio fornece insights comportamentais e proporciona controle operacional sobre a service mesh, bem como sobre os microsserviços nela. Usar uma service mesh reduz a complexidade das implantações, além de aliviar o trabalho das equipes de desenvolvimento. As funcionalidades do Istio permitem que você execute uma arquitetura de microsserviços distribuída. Esses recursos incluem:[^2]

* __Gerenciamento do tráfego__: com a configuração das regras e do roteamento do tráfego no Istio, é possível controlar o fluxo do tráfego e as chamadas de API entre serviços.[^2]

* __Segurança__: o Istio fornece um canal de comunicação subjacente, além de gerenciar a autenticação, a autorização e a criptografia da comunicação de serviços em escala. Com ele, é possível aplicar políticas de modo consistente em vários protocolos e ambientes de execução, com mudanças mínimas nas aplicações. Ao usar o Istio com políticas de rede (ou infraestrutura) do Kubernetes, os benefícios incluem a capacidade de configurar a segurança da comunicação pod a pod ou serviço a serviço nas camadas de rede e aplicação.[^2]

* __Visibilidade__: extraia insights sobre sua implantação de service mesh com as funcionalidades de rastreamento, monitoramento e geração de registros do Istio. O monitoramento permite ver como a atividade dos serviços afeta o desempenho upstream e downstream. Com os painéis personalizados, você tem visibilidade do desempenho de todos os seus serviços.
[^2]

![Istio Security Architecture](img/arch-sec.svg)
Figura 3: ___Istio Security Architecture___ [^4]

# Benefícios

Comparativo entre containers sem o uso do Istio e com Istio [^5]:

![Before Istio](img/Before_Istio.png)

![After Istio](img/after-istio.png)

# Hands On

## Traffic Management: Request Routing
* Criar o namespace: 
```bash
$ kubectl apply -f src/00-namespace.yml
```

* Fazer o deploy de uma aplicação de exemplo, echoserver:
```bash
$ kubectl apply -f src/01-deploy_v1.yaml
```

* Expor o serviço criando o Service, Gateway e VirtualService [^6]:
```bash
$ kubectl apply -f src/02-service.yaml
$ kubectl apply -f src/04-gateway.yaml
$ kubectl apply -f src/05-vistualservice.yaml
```

## Traffic Management: Traffic Shifting
* Fazer o deploy de uma nova versão do serviço:
```bash
$ kubectl apply -f src/06-deploy_v9.yaml
```

* Alterar regras de load balance [^7]:
```bash
$ kubectl apply -f src/07-destination-rule.yaml
$ kubectl apply -f src/08-virtualservice-lb.yaml
```

Gerar fluxo significativo de requisições:
```bash
$ for ((i=1;i<=10000;i++)); do   curl -k "https://ttalk.svc.fswise.com.br/echo"; done
```

Observar no [Kiali](http://k8smst001.wise.corp:32436/kiali)

## Traffic Management: Fault Injection
* Injeção de erro [^8];
```bash
$ kubectl apply -f src/09-virtualservice-fault.yaml
```

* Injeção de delay [^9].
```bash
$ kubectl apply -f src/10-virtualservice-delay.yaml
```

## Traffic Management: Routing by header
* Roteamento por cabeçalho [^10].
```bash
$ kubectl apply -f src/11-virtualservice-header.yaml
```

Exemplo de roteamento por cabeçalho:

```bash
$ curl -k https://ttalk.svc.fswise.com.br/lkjrglkdglkdfg/lwekjrflwkjrlkwer/3453453453453434-shjdfjksf0 --header "end-user: lgrissmann"
```

## Security: End-user authentication
* Adicionando autnticação com JWT. [^11]
```bash
$ kubectl apply -f src/12-policy.yaml
```

Tentativa de acesso deve retornar 401:
```bash
$ curl -k "https://ttalk.svc.fswise.com.br/echo"
```

Obtendo um token válido:
```bash
$ TOKEN=$(curl https://raw.githubusercontent.com/istio/istio/release-1.4/security/tools/jwt/samples/groups-scope.jwt -s)
```
Tentativa de acesso com um token válido deve retornar 200:
```bash
$ curl -k "https://ttalk.svc.fswise.com.br/echo" --header "Authorization: Bearer $TOKEN"
```

# Próximos passos
## Circuit Breaking
Circuit breaking for connections, requests, and outlier detection [^12].


## Distributed Tracing
Distributed tracing enables users to track a request through mesh that is distributed across multiple services. This allows a deeper understanding about request latency, serialization and parallelism via visualization[ ^13].

* Jaeger: https://www.jaegertracing.io/
* Zipkin: https://zipkin.io/
* LightStep: https://lightstep.com/


## Keycloak 
Open Source Identity and Access Management[^14]. 

## Many more

# Referências

[^1]: __MICROSSERVIÇOS - O que é service mesh?__; Disponível em: <https://www.redhat.com/pt-br/topics/microservices/what-is-a-service-mesh>. Acesso em: 4 de nov. de 2020.

[^2]: __MICROSSERVIÇOS - O que é Istio?__; Disponível em: <https://www.redhat.com/pt-br/topics/microservices/what-is-istio>. Acesso em: 4 de nov. de 2020.

[^3]: __Concepts - What is Istio?__; Disponível em: <https://istio.io/latest/docs/concepts/what-is-istio/>. Acesso em: 4 de nov. de 2020.

[^4]: __Concepts - Security__; Disponível em: <https://istio.io/latest/docs/concepts/security/#high-level-architecture>. Acesso em: 4 de nov. de 2020.

[^5]: __Kubernetes Hands-on Lab #3 – Deploy Istio Mesh on K8s Cluster__; Disponível em: <https://collabnix.com/kubernetes-hands-on-lab-3-deploy-istio-mesh/>. Acesso em: 4 de nov. de 2020.

[^6]: __Request Routing - Apply a virtual service__; Disponível em: <https://istio.io/v1.4/docs/tasks/traffic-management/request-routing/#apply-a-virtual-service>. Acesso em: 4 de nov. de 2020.

[^7]: __Traffic Shifting - Apply weight-based routing__; Disponível em: <https://istio.io/v1.4/docs/tasks/traffic-management/traffic-shifting/#apply-weight-based-routing>. Acesso em: 4 de nov. de 2020.

[^8]: __Fault Injection - Injecting an HTTP abort fault__; Disponível em: <https://istio.io/v1.4/docs/tasks/traffic-management/fault-injection/#injecting-an-http-abort-fault>. Acesso em: 4 de nov. de 2020.

[^9]: __Fault Injection - Injecting an HTTP delay fault__; Disponível em: <https://istio.io/v1.4/docs/tasks/traffic-management/fault-injection/#injecting-an-http-delay-fault>. Acesso em: 4 de nov. de 2020.

[^10]: __Request Routing - Route based on user identity__; Disponível em: <https://istio.io/v1.4/docs/tasks/traffic-management/request-routing/#route-based-on-user-identity>. Acesso em: 4 de nov. de 2020.

[^11]: __Authorization for groups and list claims__; Disponível em: <https://istio.io/v1.4/docs/tasks/security/authorization/rbac-groups/#configure-json-web-token-jwt-authentication-with-mutual-tls>. Acesso em 04 de nov. de 2020.

[^12]:__Circuit Breaking__; Disponível em: <https://istio.io/v1.4/docs/tasks/traffic-management/circuit-breaking/>. Acesso em 04 de nov. de 2020.

[^13]: __Distributed Tracing__; Disponível em: <https://istio.io/v1.4/docs/tasks/observability/distributed-tracing/>. Acesso em 04 de nov. de 2020.

[^14]: __Keycloak__; Disponível em: <https://www.keycloak.org/>. Acesso em 04 de nov. de 2020.